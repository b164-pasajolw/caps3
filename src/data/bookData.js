const bookData = [

	{
		id: "wdc001",
		title: "PHP-Laravel",
		author: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		title: "Python-Django",
		author: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		title: "Java-Springboot",
		author: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		price: 55000,
		onOffer: true
	}

]

export default bookData;