import { useEffect, useState } from 'react'
import BookCard from './../components/BookCard';
import {Row, Col, Button} from 'react-bootstrap';

//import coursesData from "./../data/coursesData"




export default function Books() {

	const [books, setBooks] = useState([])


	useEffect(() => {
		fetch("https://lit-garden-66878.herokuapp.com/api/books/activeBooks")
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setBooks(data.map(book => {
				return(
					<BookCard key={book.id} bookProp={book} />
					)
			}))

		})
	}, [])

	return(
		<Row >	
			<Col xs={12} md={12} lg={12} className="mt-5">
			<h1 className="text-center">Books</h1>,
			</Col>

			<Col className="text-center">
			{books}
			</Col>
		</Row>
		)
}