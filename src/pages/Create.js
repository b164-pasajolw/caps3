import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import UserContext from  '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Create() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate()

	//State hooks to store the values of the input fields
	const [title, setTitle] = useState("");
	const [author, setAuthor] = useState("");
	const [price, setPrice] = useState("");
	const [quantity, setQuantity] = useState("");
	
	
	// State to determine whether the button is enabled or not
	const [isActive, setIsActive] = useState(false)

	function createBook(e) {

		//prevents page redirection via a from submission
		e.preventDefault();

				fetch("https://lit-garden-66878.herokuapp.com/api/books/bookPosting", {
					method: "POST",
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`,
					},
					body: JSON.stringify({
						title: title,
						author: author,
						price: price,
						quantity: quantity
					})
				})
				.then(res => res.json())
				.then(data => {

					if(data === true) {

					//Clear input fields
					setTitle("");
					setAuthor("");
					setPrice("");
					setQuantity("");


					Swal.fire({
						title:'Book Created',
						icon: 'Success',
						text: 'New Book Posted'
					})

					.then((result) => {
					   navigate('/books')
					     })

				} else {

					Swal.fire({
						title:'Something went wrong',
						icon: 'Error',
						text: 'Please try again'
					})

				}

				})
			}
		
		


		// localStorage.setItem("email", email);

		// //Set the global user state to have properties from local storage
		// setUser({
		// 	//getItem() gettiing the email inside the localStorage
		// 	email: localStorage.getItem('email')
		// })


	

	

	useEffect(() =>{
		//Validation to enable the submit button when all the input fields are populated and both passwords match
		if((title !== "" && author !== "" && price !== "" && quantity !== "")) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
 
		}, [title, author, price, quantity]

		)

	return(

		(user.isAdmin !== true) ?

		<Navigate to="/login" />

		:

		<Form onSubmit= {(e) => createBook(e)}>
		<h1 className="text-center">Create A Book</h1>

		<Form.Group 
		  	className="mb-3"
		  	controlId="title">
			    <Form.Label>Title:</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder=" Enter Book Title"
			    	value={title}
			    	onChange={e => {
			    		setTitle(e.target.value)
			    		//console.log(e.target.value)
			    	}}
			    	required />
			   
			  </Form.Group>


				  <Form.Group 
			  	className="mb-3"
			  	controlId="author">
				    <Form.Label>Author:</Form.Label>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter Author Name"
				    	value={author}
				    	onChange={e => {
				    		setAuthor(e.target.value)
				    		//console.log(e.target.value)
				    	}}
				    	required />
				   
				  </Form.Group>


		  <Form.Group 
		  	className="mb-3"
		  	controlId="price">
			    <Form.Label>Price:</Form.Label>
			    <Form.Control 
			    	type="number" 
			    	placeholder="Enter Price"
			    	value={price}
			    	onChange={e => {
			    		setPrice(e.target.value)
			    		//console.log(e.target.value)
			    	}}
			    	required />

			  </Form.Group>



			  <Form.Group className="mb-3" controlId="quantity">
			    <Form.Label>Quantity</Form.Label>
			    <Form.Control 
			    	type="number" 
			    	placeholder="Enter Quantity" 
			    	value={quantity}
			    	onChange={e => {
			    		setQuantity(e.target.value)
			    	}}
			    	required
			    	/>


		 </Form.Group>
			  {
			  	isActive ?
			  	<Button variant="primary" type="submit" 
			  	id="submitBtn"> Submit </Button>
			  	:
			  	<Button variant="danger" type="submit" 
			  	id="submitBtn" disabled> Submit </Button>	

			  }
		</Form>
		)
}

