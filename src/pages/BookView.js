import { Fragment, useState, useEffect, useContext } from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function BookView() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	//The useParams hook allows us to retrieve the courseId passed via the URL
	const { bookId } = useParams()

	const [title, setTitle] = useState("");
	const [author, setAuthor] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);

	const buy = (bookId) => {

		fetch("https://lit-garden-66878.herokuapp.com/api/users/buy", {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				bookId: bookId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data) //true //false

			if (data === true ) {

				Swal.fire({
					title:"Order Successful!",
					icon: "success",
					text: "You have successfully ordered this book."
				})

				navigate("/books")

			} else {
				Swal.fire({
					title:"Something went wrong",
					icon: "error",
					text: "Please try again."

			})


		}
	})

}

	useEffect(() => {
		console.log(bookId)
		//to check if we are getting the courrseId on our database
		fetch(`https://lit-garden-66878.herokuapp.com/api/books/${bookId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setTitle(data.title)
			setAuthor(data.author)
			setPrice(data.price)
			setQuantity(data.quantity)
		})
	}, [bookId])

	return(
			<Container>
				<Row>
					<Col>
						<Card className="mt-5">
							<Card.Body className="text-center text-white  bg-secondary" >
								<Card.Title>{title}</Card.Title>
								<Card.Subtitle>Author:</Card.Subtitle>
								<Card.Text>{author}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>{price}</Card.Text>
								<Card.Subtitle>Quantity:</Card.Subtitle>
								<Card.Text>{quantity}</Card.Text>

	
						{ user.isAdmin === true  ?

						<Row>
							<Col xs={12} md={12} className="mb-3">
								<Button variant="primary" as={Link} to={`/update/${bookId}`}  > Update Book </Button>
							</Col>
							<Col  xs={12} md={12} className="mb-3">
								<Button variant="primary" as={Link} to={`/archive/${bookId}`}  > Deactivate </Button>
							</Col>


						</Row>
						:

								
									user.id !== null

									?

									<Button variant="primary" onClick={() => buy(bookId)}>Buy</Button>


									:
									<Link className="btn btn-danger" to="/login">Log in to Order</Link>
								
							}
								
							</Card.Body>
						</Card>
					</Col>
				</Row>

			</Container>




		)
}