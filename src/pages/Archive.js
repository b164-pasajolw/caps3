import { useState, useEffect, useContext } from 'react';
import {Container, Form, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, Navigate, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Archive() {

 const { user } = useContext(UserContext);

 const navigate = useNavigate();



 const { bookId } = useParams()

 const [title, setTitle] = useState("");
 const [author, setAuthor] = useState("");
 const [price, setPrice] = useState(0);
 const [quantity, setQuantity] = useState(0);




 const deactivate = (bookId) => {


   fetch(`https://lit-garden-66878.herokuapp.com/api/books/${bookId}/archive`, {
     method: "PUT",
     headers: {
       "Content-Type": "application/json",
       Authorization: `Bearer ${localStorage.getItem("token")}`
     },
     body: JSON.stringify({
       bookId: bookId

     })

   })
   .then(res => res.json())
   .then(data => {
     

     if (data === true) {
       Swal.fire({
         title: "Successfully Updated",
         icon: "success",
         text: "You have successfully update this item."
       })

       navigate("/books")

     } else {
       Swal.fire({
         title: "Something went wrong",
         icon: "error",
         text: "Please try again"
       })
     }
   })
 }

 useEffect(()=> {
   fetch(`https://lit-garden-66878.herokuapp.com/api/books/${bookId}`)
   .then(res => res.json())
   .then(data => {
     

     setTitle(data.title)
     setAuthor(data.author)
     setPrice(data.price)
     setQuantity(data.quantity)
   })
 }, [bookId])




   return( 
       <Container className='mt-5'>
         <Row>
           <Col lg={{span:6, offset:3}}>
             <Card>
               <Card.Body className="text-center">
                 <Card.Title> {title} </Card.Title>
                 <Card.Subtitle>Author:</Card.Subtitle>
                 <Card.Text> {author} </Card.Text>
                 <Card.Subtitle>Price:</Card.Subtitle>
                 <Card.Text> {price} </Card.Text>

               
                     <Button variant="primary" onClick= {() => deactivate(bookId)}>deactivate</Button>
                    
                 
               </Card.Body>
             </Card>
           </Col>
         </Row>
       </Container>

       )

   }
    


