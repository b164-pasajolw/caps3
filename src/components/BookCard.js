import Swal from 'sweetalert2'
import{ useState, useEffect } from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
//import UserContext from '../UserContext';



export default function BookCard({bookProp}) {

	//const { user } = useContext(UserContext);

	const {title, author, price, _id} = bookProp
	
	return(

	<Row>
		
		<Col xs={6} md={6} className="mx-auto mb-3">
			<Card className="bg-secondary">
				<Card.Body>
					<Card.Title className="text-white">{title}</Card.Title>
					<Card.Subtitle className="text-white"> Author: </Card.Subtitle>
					<Card.Text className="text-white">{author}</Card.Text>
					<Card.Subtitle className="text-white">Price:</Card.Subtitle>
					<Card.Text className="text-white">Php: {price}</Card.Text>

				{/*
					{ user.isAdmin === true  ?

						<Button variant="primary" as={Link} to={`/${_id}/updates`}  > Update A Product </Button>

						:
				*/}

					<Button variant="primary" as={Link} to={`/books/${_id}`}>Details</Button>
				
				</Card.Body>
			</Card>
		</Col>
	</Row>

		)
}
