import { Row, Col, Card } from 'react-bootstrap';


export default function Highlight() {

	return(


		<Row className="mt-3 mb-3">
			
			<Col xs={12} md={3}>
				<Card className="
				cardHighlight p-3 mt-3 ">
					<Card.Body>
						<Card.Title>"Books were my pass to personal freedom."</Card.Title>
						<Card.Text>
							- Oprah Winfrey
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>


			<Col xs={12} md={3}>
				<Card className="
				cardHighlight p-3 mt-3">
					<Card.Body>
						<Card.Title>“A room without books is like a body without a soul.”</Card.Title>
						<Card.Text>
							― Marcus Tullius Cicero
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

		
			<Col xs={12} md={3}>
				<Card className="
				cardHighlight p-3 mt-3">
					<Card.Body>
						<Card.Title>"If you don’t like to read, you haven’t found the right book."</Card.Title>
						<Card.Text>
							- J.K. Rowling
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

	
			<Col xs={12} md={3}>
				<Card className="
				cardHighlight p-3 mt-3">
					<Card.Body>
						<Card.Title>"That’s the thing about books. They let you travel without moving your feet."</Card.Title>
						<Card.Text>
							- Jhumpa Lahiri
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>



			<Col xs={12} md={4} className="mt-3" >
				<Card className="
				cardHighlight p-3">
					<Card.Body>
						<Card.Title>"Books should go where they will be most appreciated, and not sit unread, gathering dust on a forgotten shelf, don’t you agree?"</Card.Title>
						<Card.Text>
							- Christopher Paolini
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>


			
			<Col xs={12} md={4} className="mt-3" >
				<Card className="
				cardHighlight p-3">
					<Card.Body>
						<Card.Title>"If you only read the books that everyone else is reading, you can only think what everyone else is thinking."</Card.Title>
						<Card.Text>
							- Haruki Murakami
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>


			<Col xs={12} md={4} className="mt-3" >
				<Card className="
				cardHighlight p-3">
					<Card.Body>
						<Card.Title>"Reading is an exercise in empathy; an exercise in walking in someone else’s shoes for a while."</Card.Title>
						<Card.Text>
							- Malorie Blackman
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

		</Row>







		)
}