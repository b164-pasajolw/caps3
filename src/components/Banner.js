import {Row, Col, Button} from 'react-bootstrap';



export default function Banner() {

	return(

		<Row className="p-5 text-center">
			<Col>
				<h1>WELCOME TO OUR BOOKSTORE</h1>
				<h3 className="mb-3">Books For A Better You!</h3>
				<Button className="mt-3"
				variant ="primary" disabled>
					Order Now!
				</Button>
			</Col>
		</Row>

		)

}