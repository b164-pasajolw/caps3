import { useState, useEffect } from 'react'
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavbar from "./components/AppNavbar";
import Footer from './components/Footer';
import Home from './pages/Home'
import Books from './pages/Books'
import BookView from './pages/BookView'
import Register from './pages/Register'
import Create from './pages/Create'
import Update from './pages/Update'
import Login from './pages/Login'
import Admin from './pages/AdminDashboard'
import Archive from './pages/Archive'

import Logout from './pages/Logout'
import ErrorPage from './pages/Error'
import { UserProvider } from './UserContext'


import './App.css';

function App() {


//React Context is nothing but a global state to the app. It is a way to make particular data available to all the components no matter how they are nested.

  //State hook for the user state that defined here for a global scope
  //Initialized as a n object  with properties from the local storage
  //This will be uused to store the user information and will be used for validation if a user is logged in on the app or not
  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null

  })

  //Function for clearing the localStorage on logout
  const unsetUser = () =>{
    localStorage.clear()
  }

  useEffect(() => {
    
    fetch("https://lit-garden-66878.herokuapp.com/api/users/details", {
      headers: {
        Authorization:`Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin:data.isAdmin
        })
      }else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])


  return (
    <UserProvider value = {{ user, setUser, unsetUser }}>
    <Router>
      
          <AppNavbar />
              <Container>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/books" element={<Books />} />
                    <Route path="/books/:bookId" element={<BookView />} />
                    <Route path="/register" element={<Register />} />
                    <Route path="/login" element={<Login />} />
                    
                    <Route path="/create" element={<Create />} />
                    <Route path="/update/:bookId" element={<Update />} />
                    <Route path="/archive/:bookId" element={<Archive />} />
                    <Route path="/admin" element={<Admin />} />
                    <Route path="/logout" element={<Logout />} />
                    <Route path="*" element={<ErrorPage />} />
                </Routes>
              </Container>
          <Footer />
    </Router>
    </UserProvider>
  );
}

export default App;
